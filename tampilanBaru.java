package tebakgambar;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;


public class tampilan extends javax.swing.JFrame implements KeyListener{
    public static Connection con;
    public static PreparedStatement stm;
    private Database db;
 
    
    // menyimpan jawaban dari gambar
    
    public void Jawaban(){
    
    }
    String jawaban[] = {"avril_lavigne","daniel_radclife","kim_taeyeon","raditya_dika","reza_rahardian"};
    byte nyawa = 3, posisi = 0;
    int skor = 0, level = 1;
    String nama ="";
    String temp;

    public tampilan() {
        initComponents();
//        mengurutkan data yang ada di array jawaban sesuai dengan abjad
        this.db = new Database();
        sortingJawaban(jawaban);
        this.setLocationRelativeTo(null);
    }

//    subroutine untuk sorting
     public void sortingJawaban(String[] arraynya) {
        for (int i = 1; i < arraynya.length; i++) {
            int index = i;
            while (index > 0 && arraynya[index].compareToIgnoreCase(arraynya[index - 1]) < 0) {
                temp = arraynya[index - 1];
                arraynya[index - 1] = arraynya[index];
                arraynya[index] = temp;
                index--;
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtJawaban = new javax.swing.JTextField();
        btnCek = new javax.swing.JButton();
        background = new javax.swing.JPanel();
        lblSkor = new javax.swing.JLabel();
        lblNyawa = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        gambar = new javax.swing.JLabel();
        lblLevel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(102, 255, 0));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMinimumSize(new java.awt.Dimension(500, 470));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtJawaban.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        txtJawaban.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtJawaban.setBorder(null);
        txtJawaban.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtJawaban.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtJawabanActionPerformed(evt);
            }
        });
        txtJawaban.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtJawabanKeyPressed(evt);
            }
        });
        getContentPane().add(txtJawaban, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 330, 300, 30));

        btnCek.setBackground(new java.awt.Color(255, 255, 255));
        btnCek.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnCek.setText("JAWAB");
        btnCek.setBorder(null);
        btnCek.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCek.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pop(evt);
            }
        });
        btnCek.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCekActionPerformed(evt);
            }
        });
        btnCek.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                btnCekKeyTyped(evt);
            }
        });
        getContentPane().add(btnCek, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 330, 79, 30));

        background.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblSkor.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        lblSkor.setForeground(java.awt.Color.white);
        lblSkor.setText("Skor : 0 ");
        background.add(lblSkor, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 20, -1, -1));

        lblNyawa.setBackground(new java.awt.Color(102, 102, 102));
        lblNyawa.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        lblNyawa.setForeground(new java.awt.Color(255, 255, 255));
        lblNyawa.setText("Nyawa : 3");
        background.add(lblNyawa, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(13, 13, 13));
        jLabel1.setText("Jawaban : ");
        background.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 370, -1, -1));

        gambar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tebakgambar/UI/avril_lavigne.png"))); // NOI18N
        background.add(gambar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 110, -1, -1));

        lblLevel.setFont(new java.awt.Font("Goudy Stout", 0, 14)); // NOI18N
        lblLevel.setForeground(java.awt.Color.white);
        lblLevel.setText("level 1");
        background.add(lblLevel, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 20, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tebakgambar/UI/Background.jpg"))); // NOI18N
        background.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 530, 400));

        getContentPane().add(background, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 530, 400));

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void txtJawabanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtJawabanActionPerformed
        txtJawaban.addKeyListener(this);
    }//GEN-LAST:event_txtJawabanActionPerformed

    private void btnCekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCekActionPerformed
        
        String jwb = txtJawaban.getText().replace(" ", "_");
        
        if (jwb.equalsIgnoreCase("")) {
            JOptionPane.showMessageDialog(this, "ISI JAWABAN");
            txtJawaban.requestFocus();
        } else {
            if (jwb.equalsIgnoreCase(jawaban[posisi])) {
                JOptionPane.showMessageDialog(this, "BENAR");
                txtJawaban.requestFocus();
                posisi++;
                skor += 10;
                level += 1;
                lblSkor.setText("Skor : " + skor);
                lblLevel.setText("LEVEL " + level);
                
                txtJawaban.setText("");
            } else {
                JOptionPane.showMessageDialog(this, "SALAH");
                nyawa--;
                txtJawaban.requestFocus();
                lblNyawa.setText("Nyawa : " + nyawa);
            }
        }

        // cek nyawa
        if (nyawa == 0) {
            
    
            
            JOptionPane.showMessageDialog(this, "KALAH");
            JOptionPane.showMessageDialog(this, "SKOR ANDA = " + skor);
            nama = JOptionPane.showInputDialog("Nama Anda");
            String kampret = Integer.toString(skor);
            this.db.addData(nama, kampret);
//            //Database db = new Database();
//            try {
//                String sql = "INSERT INTO nama (name) VALUES('"+nama+"')";
//                stm = db.getConn().getKoneksi().prepareStatement(sql);
//                
//                stm.execute();
//             //   stm.execute("INSERT INTO skor (skor) VALUES('"+skor+"')");
//                JOptionPane.showMessageDialog(this, "Berhasil Di Simpan");
//            } catch (SQLException ex) {
//                JOptionPane.showMessageDialog(this, "Gagal Di Simpan");
//            }
//            //db.insertTabel(nama,skor);

            
            
            skor tskor = new skor(db);
            this.dispose();
        }

        // cek posisi
        if (posisi > jawaban.length - 1) {
            JOptionPane.showMessageDialog(this, "SELAMAT ANDA MENANG");
            JOptionPane.showMessageDialog(this, "SKOR ANDA = " + skor);
            nama = JOptionPane.showInputDialog("Nama Anda");
           //Database db = new Database();
           String kampret = Integer.toString(skor);
           db.addData(nama, kampret);
//            try {
//                stm.executeUpdate("INSERT INTO nama (name) VALUES('"+nama+"')");
//                stm.executeUpdate("INSERT INTO skor (skor) VALUES('"+skor+"')");
//                JOptionPane.showMessageDialog(this, "Berhasil Di Simpan");
//            } catch (SQLException ex) {
//                JOptionPane.showMessageDialog(this, "Gagal Di Simpan");
//            }
//            new skor().setVisible(true);
        skor tskor = new skor(db);
            this.dispose();
        } else {
            ImageIcon myimage = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/tebakgambar/UI/"+jawaban[posisi]+".png")));
            
            Image img1 = myimage.getImage();
            Image img2 = img1.getScaledInstance(gambar.getWidth(), gambar.getHeight(), Image.SCALE_SMOOTH);
            
            ImageIcon i = new ImageIcon(img2);
            gambar.setIcon(i);
            //gambar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tebakgambar/UI/" + jawaban[posisi] + ".png")));
        }


    }//GEN-LAST:event_btnCekActionPerformed

    private void pop(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pop
        // TODO add your handling code here:
    }//GEN-LAST:event_pop
    private void txtJawabanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtJawabanKeyPressed
         if(evt.getKeyCode()==KeyEvent.VK_ENTER){
             String jwb = txtJawaban.getText().replace(" ", "_");
        if (jwb.equalsIgnoreCase("")) {
            JOptionPane.showMessageDialog(this, "ISI JAWABAN");
            txtJawaban.requestFocus();
            
        } else {
            if (jwb.equalsIgnoreCase(jawaban[posisi])) {
                JOptionPane.showMessageDialog(this, "BENAR");
                txtJawaban.requestFocus();
                posisi++;
                skor += 10;
                level += 1;
                lblSkor.setText("Skor : " + skor);
                lblLevel.setText("LEVEL " + level);
                
                txtJawaban.setText("");
            } else  {
                JOptionPane.showMessageDialog(this, "SALAH");
                nyawa--;
                txtJawaban.requestFocus();
                lblNyawa.setText("Nyawa : " + nyawa);
            }
        }

        // cek nyawa
        if (nyawa == 0) {
            JOptionPane.showMessageDialog(this, "KALAH");
            JOptionPane.showMessageDialog(this, "SKOR ANDA = " + skor);
            nama = JOptionPane.showInputDialog("Nama Anda");
            int id=1;
           
            
            
            String kampret = Integer.toString(skor);
            this.db.addData(nama, kampret);
                //stm.execute("INSERT INTO skor (skor) VALUES('"+skor+"')");
                
                JOptionPane.showMessageDialog(this, "Berhasil Di Simpan");
         
            skor tskor = new skor(db);
            this.dispose();
        }

        // cek posisi
        if (posisi > jawaban.length-1) {
            JOptionPane.showMessageDialog(this, "SELAMAT ANDA MENANG");
            JOptionPane.showMessageDialog(this, "SKOR ANDA = " + skor);
            nama = JOptionPane.showInputDialog("Nama Anda");
//            Database db = new Database();
             String kampret = Integer.toString(skor);
            this.db.addData(nama, kampret);
            JOptionPane.showMessageDialog(this, "Berhasil Di Simpan");
            skor tskor = new skor(db);
              
         
            this.dispose();
        } else {
            ImageIcon myimage = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/tebakgambar/UI/"+jawaban[posisi]+".png")));
            
            Image img1 = myimage.getImage();
            Image img2 = img1.getScaledInstance(gambar.getWidth(), gambar.getHeight(), Image.SCALE_SMOOTH);
            
            ImageIcon i = new ImageIcon(img2);
            gambar.setIcon(i);
            //gambar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tebakgambar/UI/" + jawaban[posisi] + ".png")));
        }
        } 
    }//GEN-LAST:event_txtJawabanKeyPressed

    private void btnCekKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCekKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCekKeyTyped

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new tampilan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel background;
    private javax.swing.JButton btnCek;
    private javax.swing.JLabel gambar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblLevel;
    private javax.swing.JLabel lblNyawa;
    private javax.swing.JLabel lblSkor;
    private javax.swing.JTextField txtJawaban;
    // End of variables declaration//GEN-END:variables

    

    @Override
    public void keyPressed(KeyEvent evt) {
         if(evt.getKeyCode()==KeyEvent.VK_ESCAPE){
            int jawab = JOptionPane.showConfirmDialog(this, "YAKIN INGIN KELUAR?");
             //0=yes,1=no,2=cancel
             switch(jawab){
        case JOptionPane.YES_OPTION: 
            dispose();
            break;  
            }  
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
