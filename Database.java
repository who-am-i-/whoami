package tebakgambar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;
import tebakgambar.Koneksi;

public class Database {

    private Connection conn;
    public Database() {
	connect();
		
    }
    
//    Koneksi connec = new Koneksi();
//    Connection con= connec.getKoneksi();
//    PreparedStatement stm;
//    ResultSet rs;
//
//    public DefaultTableModel isiTabel() {
//        // query ambil data
//        String getContent = "SELECT name,skor FROM skor ORDERBY LIMIT 10";
//
////        mendeklarasikan nama kolom
//        Object header[] = {"name", "skor"};
//
////        deklarasi model tabel
//        DefaultTableModel TableData = new DefaultTableModel(null, header) {
//            @Override
//            public boolean isCellEditable(int row, int column) {
//                return false;
//            }
//        };
//
//        try {
//            PreparedStatement stm = con.prepareStatement(getContent);
//
////            eksekusi query
//            rs = stm.executeQuery(getContent);
//
//            while (rs.next()) {
//                String Data[] = {rs.getString(1), rs.getString(2)};
//                TableData.addRow(Data);          
//            }
//
////            menutup koneksi
//            rs.close();
//            stm.close();
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(null, "Kesalahan : " + e, "Bug!", JOptionPane.INFORMATION_MESSAGE);
//        } finally {
//            return TableData;
//        }
//    }
//
//    public void insertTabel(String player, int skor) throws SQLException {
//            
//        // query ambil data
//        String save = "SELECT idplayer FROM nama LEFT JOIN skor ON  name = '"+player+"';";
//
//        try {
//            stm = con.prepareStatement(save);
//            
////            eksekusi query
//            rs=stm.executeQuery();
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(null, "Kesalahan : " + e, "Bug!", JOptionPane.INFORMATION_MESSAGE);
//        }
//        while(rs.next()){
//            int id = rs.getInt("idplayer");
//            // query ambil data
//        String save2 = "INSERT INTO `skor` (`idplayer`,`name`, `skor`) VALUES ('"+id+"','" + player + "', '" + skor + "');";
//
//        try {
//            PreparedStatement stm = con.prepareStatement(save2);
//            
////            eksekusi query
//            stm.execute();
//
//            stm.close();
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(null, "Kesalahan : " + e, "Bug!", JOptionPane.INFORMATION_MESSAGE);
//        }
//        }
//        
//
//    }
//
//    void insertTabel(String nama) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    void insertTabel(int skor) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//    
//    public Koneksi getConn(){
//        return connec;
//    }
    
    private void connect() {

		SwingWorker<Void, Void> konak = new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {
				try {
					conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/tebakgambar?serverTimezone=UTC", "root",
							"");
					if (conn != null) {
						System.out.println("Connected to Database");
					}

				} catch (SQLException e) {
					System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
				} catch (Exception e) {
					e.printStackTrace();
				}

				return null;
			}

		};

		konak.execute();

    }
    public Connection getConn() {
		return conn;
	}
    
	public void addData(String nama, String skor) {
		SwingWorker<Void, Void> konak = new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {
			
				
				String sqll = "INSERT INTO nama (name) VALUES('"+nama+"')";
                                
				String select = "select idplayer from nama where name = '"+nama+"';";
				
				try {
					
					PreparedStatement preparedStatement = conn.prepareStatement(sqll);
					
					preparedStatement.execute();
                                        
					
					
					preparedStatement = conn.prepareStatement(select);
                                        
                                        ResultSet result = preparedStatement.executeQuery();
                                        
                                        result.next();
                                        
                                        String kampret = result.getString("idplayer");
                                        
                                        String sql2 = "INSERT INTO skor (idplayer, skor, name) VALUES('"+kampret+"','"+skor+"','"+nama+"')";
                                        
                                        preparedStatement = conn.prepareStatement(sql2);
					
					preparedStatement.execute();
//					preparedStatement = conn.prepareStatement(sql);
//					preparedStatement.execute();
					System.out.println("berhasil");
				} catch (SQLException e) {
					System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

		};

		konak.execute();

	}
	
	
	public void getData(skor skor) {
		
		SwingWorker<Void, Void> konak;
        konak = new SwingWorker<Void, Void>() {
            
            @Override
            protected Void doInBackground() throws Exception {
                
                String sql = "SELECT nama.name, skor FROM `skor` LEFT join nama on nama.idplayer = skor.idplayer order by skor desc limit 10";
                
                
                try {
                    
                    PreparedStatement preparedStatement = conn.prepareStatement(sql);
                    
                    ResultSet result = preparedStatement.executeQuery();
                    
                    while(result.next()) {
                        
                        String nama = result.getString("name");
                        String score = result.getString("skor");
                        
                        skor.getIsi().addRow(new Object[] {nama, score});
                        
                        
                    }
                    System.out.println("berhasil");
                } catch (SQLException e) {
                    System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
            
        };

		konak.execute();
		
	}

   

}
